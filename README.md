# README #

### Setup ###

In same directory as build.xml, run the following commands:

```
ant compile
ant jar
ant run
```

### Team Responsibilities ###

* See files in src/threesolid for authorship and justification for the seperation of files.