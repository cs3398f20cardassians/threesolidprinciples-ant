package threesolid;

/**
 * @author Brandon Burtchell
 * 
 * This interface was created under the Interface Segregation Principle in order to lighten the
 * original IWorker interface and to allow its extension/implementation by 'workable'-only classes.
 */
interface IWorkable {
  public void work();
}