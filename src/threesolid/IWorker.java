package threesolid;

/**
 * @author Brandon Burtchell
 * 
 * This interface is seperated/retained to offer extension under the Open Close Principle. See
 * IFeedable, IWorkable for an example of Interface Segregation Principle.
 */
interface IWorker extends IFeedable, IWorkable {
}