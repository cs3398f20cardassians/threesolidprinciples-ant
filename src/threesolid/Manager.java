package threesolid;

/**
 * @author Tanya Garduno
 * 
 * This class was created under the Interface Segregation Principle to
 * give the manager class access to the method it only needs to access.
 * - set worker,
 * - void manage
 * - and IWorker worker.
 */
class Manager {
  IWorker worker;

  public void Manager() {

  }

  public void setWorker(IWorker w) {
    worker = w;
  }

  public void manage() {
    worker.work();
  }
}
