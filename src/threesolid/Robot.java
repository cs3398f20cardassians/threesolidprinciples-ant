package threesolid;

/**
 * @author Allison Spitzenberger
 * 
 * This interface was created using the Interface Segregation Principle as the user should not
 * be forced to implement methods that is not required for a desired purpose
 * IWorker has both work()and eat() functions, so creating a new class does not force
 * implementation of the eat() function.
 */
class Robot implements IWorkable{
	public void work() {
		// ....working
	}
}