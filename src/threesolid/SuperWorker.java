package threesolid;

/**
* @author Julian Sherlock
*
* This class was designed to implement thin, focused interfaces (Interface Segregation 
* Principle), which allows the class to not be forced into depending on methods it 
* doesn't use.
* 
* This class was also designed to have one responsibility under the Single Responsibility
* Principle. This new class implements various interfaces which now have very little 
* reason to change
*
* The class is an extension of the software, where the software it extends from is 
* open for extension(such as this class) but closed for modification. This is the 
* Open Close Principle, and therefore allows new functionality to be added while 
* keeping existing code unchanged as much as possible.
*/
class SuperWorker implements IWorkable, IFeedable{
	public void work() {
		//.... working much more
	}

	public void eat() {
		//.... eating in launch break
	}
}