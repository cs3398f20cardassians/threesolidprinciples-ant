package threesolid;

import java.awt.*; // Using AWT container and component classes
import java.awt.event.*; // Using AWT event classes and listener interfaces
import java.io.*;

/**
 * @author Connor Scott
 * 
 *         This class was designed to be the body of the program. Inside of the
 *         class TreeSolidMain, the main function takes place. In the
 *         ThreeSolidMain class, a tsManager var of the Manager type, which is
 *         part of a different file.
 *
 *         The responsibility of the ThreeSolidMain class is to check different
 *         states and output an exception if something is off.
 *
 *         This file should follow the Open-Close Principle if it was to be
 *         changed in the future because this file is open to extension but
 *         closed to modification. If this file was to be changed in the future,
 *         the purpose of this class should not change, only added to.
 *
 *         This follows the Interface Segregation Principle as it does not
 *         intertwine the other files together to where removing one would make
 *         the file useless. In other words, the file does not depend on methods
 *         it does not use.
 */
public class ThreeSolidMain {

  public static Manager tsManager = new Manager();

  // The entry main() method
  public static void main(String[] args) {

    try {
      System.out.format("Starting ... \n");
    } catch (Exception main_except) {
      main_except.printStackTrace();
    }

    try {
      System.out.format("Stopping ... \n");
    } catch (Exception main_except) {
      main_except.printStackTrace();
    }

    System.exit(0);
  }
}
